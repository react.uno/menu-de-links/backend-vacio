let router = require('express').Router();

let userController = require('./controllers/userController');
// Contact routes
router.route('/users')
    .get(userController.listUsers)
    .post(userController.createUser);
router.route('/users/:user_id')
    .get(userController.getUser)


// Export API routes
module.exports = router;