// Import contact model
User = require('../models/userModel');
// Handle index actions
const listUsers = function (req, res) {
    User.get(function (err, users) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Users retrieved successfully",
            data: users
        });
    });
};
// Handle create contact actions
const createUser = (req, res) => {
    var user = new User();
    user.name = req.body.name ? req.body.name : res.json({message: "Ta mal"});
    user.links = req.body.links ? req.body.links : res.json({message: "Ta mal"});
    // save the contact and check for errors
    user.save(function (err) {
        // if (err)
        //     res.json(err);
        res.json(user);
    });
};
// Handle view contact info
const getUser = function (req, res) {
    // let value = Number(req.params.user_id);
    // if(!value) return listUsers(req, res);
        User.findById(req.params.user_id, function (err, user) {
            if (err)
                return res.status(404).send(JSON.stringify(err));        

            res.json(user);
        });        

};
module.exports = {
    createUser,
    getUser,
    listUsers
};
